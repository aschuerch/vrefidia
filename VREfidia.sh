#!/bin/bash -i

## uncomment these settings for debugging
#set -e
#set -v
#set -x

########################################
##Script to call snakefile for bacterial paired-end WGS Illumina data, optimized for VRE
## Run on a HPC with SLURM scheduler by requesting a node with sufficient memory 
## screen -S [session_name]
## srun --time=01:00:00 --mem=32G --gres=tmpspace:4 --pty bash
##aschuerch 09-2021
########################################


##1. Checks
##Check for command line arguments

if [ $# -eq 0 -o "$1" == "-h" -o "$1" == "--help" ]; then
    echo "
###########################################################################
############      Basic microbial WGS analysis pipeline    ################
##                                                                       ##
## for all samples in this folder, optimized for VRE.                    ##
##                                                                       ##
## Untrimmed, paired end, compressed sequencing files (fastq.gz)         ##
## must be present in the same folder from where the script is called.   ##
## Takes all sequencing length, Illumina-sequenced, paired-ends reads    ##
##                                                                       ##
## Example:                                                              ##
##                                                                       ##
## ./VREfidia.sh  ENF-RES-PR1-00001_R1.fastq.gz ENF-RES-PR1-00001_R2.fastq.gz
##  ENF-RES-PR1-00002_R1.fastq.gz ENF-RES-PR1-00002_R2.fastq.gz          ##
##                                                                       ##
## or                                                                    ##
##                                                                       ##
## ./VREfidia.sh ALL                                                   ##
##                                                                       ##
## Packages and versions are specified in envs/packages.yml.             ## 
## See bioconda.github.io for available packages.                        ##
## Command line parameters for individual tools can be adjusted in       ##
## config/config.yaml                                                    ##
##                                                                       ##
## Version Oct2021                                                     ##
###########################################################################"
    exit
fi

## create logs

mkdir -p "$(pwd)"/log
log=$(pwd)/log/call_assembly.txt
touch "$log"
sleep 1

## Check for *fastq.gz files


if [ "$1" == """ALL""" ];then
   echo "All fastq.gz files will be processed"  2>&1 | tee -a "$log"
   files=(./*fastq.gz)
else
   echo else
   files=( "$@" )
fi


for file in "${files[@]}"
do
  if [ -e "$file" ]
   then   # Check whether file exists.
     echo 'Found files for ' "$file"  2>&1 | tee -a "$log"
   else
     echo 'Sequence files as '"$file"'_*1.fastq.gz are missing in this folder.
  Please execute this script from the location of the sequencing files or exclude the sample.
 Exiting.' 2>&1 | tee -a "$log"
 exit 1
   fi
done

#Check if conda is installed, if not found attempt to install in a temporary folder

if command -v conda > /dev/null; then
 echo  2>&1| tee -a "$log"
 echo 
 echo "conda found" | tee -a "$log"
else
 echo
 echo "conda missing."
 echo "Install Miniconda with:" 
 echo
 echo "    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh"
 echo "    chmod +x Miniconda3-latest-Linux-x86_64.sh"
 echo "    ./Miniconda3-latest-Linux-x86_64.sh"
 echo "and follow the prompts."
 echo "After installation, configure the channels with"
 echo
 echo "    conda config --add channels defaults"
 echo "    conda config --add channels bioconda"
 echo "    conda config --add channels conda-forge"
 exit 1
fi

echo "The logfiles will be generated here: " 2>&1 | tee -a "$log"
echo "$(pwd)"/log  2>&1| tee -a "$log"
echo 2>&1 |tee -a "$log"
sleep 1

# determine read length

length=$(zcat "${files[0]}" | awk '{if(NR%4==2) print length($1)}' | sort | uniq -c | sort -rn | head -n 1 | rev | cut -f 1,1 -d " "| rev)

# Write to log
echo 2>&1 |tee -a "$log"
echo "Read length was determined as: " 2>&1| tee -a "$log"
echo "$length" 2>&1| tee -a "$log"

# Check if snakemake is found or install directly into base 
if command -v snakemake > /dev/null; then ##version?
echo 2>&1 |tee -a "$log"
echo "snakemake found" 2>&1 |tee -a "$log"
else
echo 2>&1 |tee -a "$log"
echo "snakemake will be installed" 2>&1 |tee -a "$log"
conda install -y snakemake=5.14.0
fi


sleep 1
 
# concatenate forward and reverse and put into data/ folder:

mkdir -p data

for file in "${files[@]}"
 do
  i="${file%%_*}"
  echo "$i"
  cat "$i"*1.fastq.gz > data/"$i"_R1.fastq.gz
  cat "$i"*2.fastq.gz > data/"$i"_R2.fastq.gz 
 done


# run the snakemake pipeline

echo "snakemake --snakefile Snakefile.assembly --use-conda --cores all --printshellcmds --latency-wait 60 --keep-going --config length=$length"
snakemake --snakefile Snakefile.assembly --use-conda --cores all --printshellcmds --latency-wait 60 --keep-going --config length="$length" | tee -a "$log"

#for the CI
if [ $? -eq 0 ]
then
  echo "Successfully finished job"
  exit 0
else
  echo "Could not finish job" >&2
  exit 1
fi

