# VREfidia
                                                                                                                                                                             
<img src="noun_Snake_blue.png" width="300px" style="display: block; margin: auto;" />

Primary repo and development on https://gitlab.com/aschuerch/vrefidia.git

### Basic microbial WGS analysis pipeline

*VREfidia* is a bacterial assembly and basic analysis pipeline for vancomycin-resistant Enterococcus faecium (VRE) using Snakemake and bioconda.
It is designed for paired-end Illumina data. The pipeline is written to ensure reproducibility, 
and creates virtual software environments with the software versions that are used for analysis.

## Dependencies

VREfidia runs under bash and relies on software available from [bioconda](https://bioconda.github.io) and a (mini)conda installation. 
If conda is not present, the script will suggest a miniconda installation.

## Usage 

Clone this repository with

```bash
git clone -b VREQUICK https://gitlab.com/aschuerch/vrefidia.git VREfidia_[myproject]
```

where [myproject] is the name of your project.

Copy or symlink your paired-end read sequencing files
(Sample1_R1.fastq.gz, Sample1_R2.fastq.gz, Sample2_R1.fastq.gz and Sample2_R2.fastq.gz) to the VREfidia_[myproject] directory.
After succesful execution of the pipeline, the files will be copied to the results directory.


The first underscore in the sample names is regarded as the delimiter for the sample name.
Avoid other underscores in the sample names.

Run the pipeline with


```bash
./VREfidia.sh Sample1_R1.fastq.gz Sample1_R2.fastq.gz Sample2_R1.fastq.gz Sample2_R2.fastq.gz
```
or 

```bash
./VREfidia.sh ALL
```

The pipeline takes Illumina paired-end sequencing reads as compressed sequencing files (.fastq.gz) 
which must be present in the same directory from where the script is run.

The different versions of the packages that are run are defined in the `envs/` directory. 

The first time VREfidia is run, it generates all virtual environments which can take a considerable time depending on the speed of your internet connection.
**Do not interrupt this process!**


## De-bugging and testing

For debugging or testing purposes, the pipeline itself can be dry-run with 

```bash
./dryrun_VREfidia.sh
```

The pipeline takes compressed sequencing files (.fastq.gz) which must be 
present in the same directory from where the script is called.

Test the whole pipeline with:

```bash
ln -s  test/Test*gz .
./VREfidia.sh Test_R1.fastq.gz Test_R2.fastq.gz
```

This will run the pipeline on the included test files.

## Analysis steps

Currently it runs:
 - trimming with [trimgalore](http://bioconda.github.io/recipes/trimgalore/README.html)
 - downsampling to about 25x
 - assembly with [skesa](http://bioconda.github.io/recipes/skesa/README.html)
 - mlst with the efaecium mlst scheme with [mlst](http://bioconda.github.io/recipes/mlst/README.html)

Running only 

```bash
./VREfidia.sh
```

will give an explanation of the (limited) options.


## Output

The output can be found in the 'results' directory which contains the following files representing the output of the different tools

```
[timestamp]_results/
│
├── Test_R1.fastq.gz # input file 1
├── Test_R2.fastq.gz # input file 2
│ 
├── envs
│   ├── ... # contains all environment definitions, e.g. used versions of programs
│ 
├── output
│   ├── Test.fna   # scaffolds
│   ├── MLST.tsv # MLST results in table format
│   ├── Test_R1_val_1.fq # trimmed reads, pair 1
│   ├── Test_R2_val_2.fq # trimmed reads, pair 2
│   ├── Test_R1_fastq.gz_trimming_report.txt # report of trimgalore
│   ├── Test_R2_fastq.gz_trimming_report.txt # report of trimgalore 
│
└── log
    ├── call_assembly.txt # log file
```


## Using different package versions

Package versions can be adjusted in envs/*yaml. 
The packages are in different files, mainly due to different dependencies such as python 2 / python 3.
Please visit [bioconda](http://bioconda.github.io/) for available packages.


## Adding other tools

For further customizing, see [snakemake documentation](https://snakemake.readthedocs.io/en/stable/)


## Trouble shooting

Sometimes the snakemake workflow will give you an error due to an already running instance.
In this case, unlock the snakemake instance with

```bash
./unlock_VREfidia.sh
```
The pipeline can be tested with

```bash
./dryrun_VREfidia.sh
```

To re-run parts of the pipeline, try

```bash
./rerun_incomplete_VREfidia.sh
```
